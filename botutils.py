# Helpers
# TODO: Use config file for perms
import re
import random
import requests


def is_admin(message):
    return message.channel.permissions_for(message.author).administrator


def is_mod(message):
    return message.channel.permissions_for(message.author).kick_members


def random_line_url(url):
    nopes_req = requests.get(url)
    nopes = nopes_req.content
    return random.choice(nopes.split(b"\n")).decode('utf-8')


def html_md(format_me):
    # Escape * and _
    format_me = re.sub(r'\*', '\\*', format_me)
    format_me = re.sub(r'_', '\\_', format_me)
    # Bold
    format_me = re.sub(r'<.?b>', '**', format_me)
    # Italic
    format_me = re.sub(r'<.?i>', '_', format_me)
    # Underline
    format_me = re.sub(r'<.?u>', '__', format_me)
    # New lines
    format_me = re.sub(r'<br>', '\n', format_me)
    # Links
    format_me = re.sub(r'<a href="(.*)">(.*)</a>', '[\\2](\\1)', format_me)
    return format_me


async def del_message(self, ctx):
    if ctx.message.server:
        await self.bot.delete_message(ctx.message)
