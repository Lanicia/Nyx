import os
import random
import discord
import sqlite3
import requests
import configparser
import better_exceptions

from shutil import copyfile
from botutils import is_mod
from datetime import datetime
from discord import utils as dutils
from discord.ext import commands
from dateutil.relativedelta import relativedelta

# Setup config
conf = configparser.ConfigParser()
conf.read('./config.ini')

# Setup discord-stuff
description = '"Dévouée a servir la Princess Luna dans la Discorde."'
bot = commands.Bot(command_prefix='!', description=description)

# init
better_exceptions.MAX_LENGTH = None
db = None
banlist = []


# On bot login
@bot.event
async def on_ready():
    print('Bonjour, moi c\'est ' + bot.user.name + ' !')
    print('Lien d\'invitation : https://discordapp.com/oauth2/authorize?permissions=872803526&scope=bot&client_id=' + bot.user.id)
    print('------------------')

    # Import our 'modules'
    bot.load_extension('utilitaires')
    bot.load_extension('fun')
    bot.load_extension('mod')

    # Load banlist
    banreq = requests.get('https://bans.discordlist.net/api')
    for i in banreq.json():
        banlist.append(str(i[0]).strip())
    print('Liste publique de bans chargée !')


# On new messages
@bot.event
async def on_message(message):
    low_message = message.content.lower()
    mention = message.author.mention

    # Remove ads
    no_ads = ['discord.gg', 'discordapp.com/invite']
    if any(thing in low_message for thing in no_ads) and not is_mod(message):
        await bot.delete_message(message)
        return

    # Trigger by words and sentences
    quote_42 = ['réponse à la vie, l\'univers et tout le reste', 'answer to life the universe and everything']
    if any(thing in low_message for thing in quote_42):
        await bot.send_message(message.channel, '42 <:classyluna:302196645980930049>')

    if 'pain au chocolat' in low_message and 'chocolatine' not in low_message:
        if random.randint(0, 3) > 1:
            await bot.send_message(message.channel, '*chocolatine <:superior:302208042294837264>')

    if 'linux' in low_message and 'gnu' not in low_message:
        if random.randint(0, 5) > 3:
            await bot.send_message(message.channel, mention + ': "I would just like to interject for a moment." Ce que vous semblez ' +
                                   'faire référence à Linux est, en réalité, GNU/Linux ou comme j\'ai commencé à le nommer récemment, ' +
                                   'GNU plus Linux.')

    if 'rt si c trist' in low_message:
        await bot.send_message(message.channel, 'rt %s' % mention)

    if 'talk dirty to me' in low_message:
        await bot.send_message(message.channel, '%s: _\\*prends sa trompette\\*_ "Tululu tutulu lulu lulululu"' % mention)

    if '>>>/' in low_message:
        board = low_message.split('>>>/', 1)[1].split('/', 1)[0]
        print(board)
        sfw_chans = ['3', 'a', 'adv', 'an', 'asp', 'biz', 'c', 'cgl', 'ck', 'cm', 'co', 'diy', 'fa', 'fit', 'g', 'gd', 'his', 'int',
                     'jp', 'k', 'lgbt', 'lit', 'm', 'mlp', 'mu', 'n', 'news', 'o', 'out', 'p', 'po', 'qa', 'qst', 'sci', 'sp', 'tg',
                     'toy', 'trv', 'tv', 'v', 'vg', 'vip', 'vp', 'vr', 'w', 'wsg', 'wsr', 'x']
        nsfw_chans = ['aco', 'b', 'd', 'e', 'f', 'gif', 'h', 'hc', 'hm', 'hr', 'i', 'ic', 'pol', 'r', 'r9k', 's', 's4s', 'soc', 't',
                      'trash', 'u', 'wg', 'y']

        if any(thing == board for thing in sfw_chans):
            await bot.send_message(message.channel, 'https://boards.4chan.org/%s/' % board)
        elif any(thing == board for thing in nsfw_chans):
            await bot.send_message(message.channel, 'https://boards.4chan.org/%s/ **(NSFW)**' % board)

    # Mentionning the bot
    if any(thing.id == bot.user.id for thing in message.mentions):
        hellos = ['salut', 'coucou', 'hello', 'bonjour', 'plop']

        if '?' == low_message[-1:]:
            if random.randint(0, 20) > 16:
                await bot.send_message(message.channel, mention + ': non')
            else:
                await bot.send_message(message.channel, mention + ': oui')
        elif any(thing in low_message for thing in hellos):
                await bot.send_message(message.channel, 'Coucou ' + mention + ' <:party:302202991019163658>')

    # Log that the user was active
    curr_datetime = datetime.now()
    with db:
        ua_cur = db.cursor()
        ua_cur.execute("INSERT OR REPLACE INTO useractivity(uaUser, uaMsg, uaDate) VALUES (?,?,?)",
                       (message.author.id, message.content, curr_datetime))

    # Launch commands if there's any
    await bot.process_commands(message)


# On user join
@bot.event
async def on_member_join(member):
    muted = False

    # Check if the user is in a ban list
    if str(member.id) in banlist:
        # mute him if the server can do it
        try:
            muteid = conf.get('roles', 'mute')  # role id to mute
            role = dutils.get(member.server.roles, id=muteid)
            await bot.add_roles(member, role)
            muted = True
        except:
            raise
            pass

    # Notify in defined channel for the server
    try:
        chan = conf.get('chans', 'modlogs')  # get channel id who gets mod logs
    except configparser.NoOptionError:
        return

    if chan is None:
        return  # If there's nothing, don't do anything

    # Build an embed
    if muted:
        em = discord.Embed(title=member.name + ' à rejoint et est mute.', description='**NOTE** : ' + member.mention + ' est auto-mute.',
                           colour=0x23D160, timestamp=datetime.utcnow())  # color: green
    else:
        em = discord.Embed(title=member.name + ' à rejoint.', description='Dites bonjour à ' + member.mention,
                           colour=0x23D160, timestamp=datetime.utcnow())  # color: green

    em.set_thumbnail(url=member.avatar_url)
    em.set_footer(text='ID: ' + str(member.id))

    # Send message with embed
    await bot.send_message(discord.Object(int(chan)), embed=em)


# On user leave
@bot.event
async def on_member_remove(member):
    # Notify in defined channel for the server
    try:
        chan = conf.get('chans', 'modlogs')  # get channel id who gets mod logs
    except configparser.NoOptionError:
        return

    if chan is None:
        return  # If there's nothing, don't do anything

    # Count for how long an user has been a member
    diff = relativedelta(datetime.utcnow(), member.joined_at)
    member_since = member.mention + ' était membre pendant %d mois, %d jours, %d heures, %d minutes et %d secondes.' % (
                   diff.months, diff.days, diff.hours, diff.minutes, diff.seconds)

    # Build an embed
    em = discord.Embed(title=member.name + ' a quitté.', description=member_since,
                       colour=0xE81010, timestamp=datetime.utcnow())  # color: red
    em.set_thumbnail(url=member.avatar_url)
    em.set_footer(text='ID: ' + str(member.id))

    # Send message with embed
    await bot.send_message(discord.Object(int(chan)), embed=em)


# On user ban
@bot.event
async def on_member_ban(member):
    # Notify in defined channel for the server
    try:
        chan = conf.get('chans', 'modlogs')  # get channel id who gets mod logs
    except configparser.NoOptionError:
        return

    if chan is None:
        return  # If there's nothing, don't do anything

    # Build an embed
    em = discord.Embed(title=member.name + ' est banni du serveur.',
                       colour=0x7289DA, timestamp=datetime.utcnow())  # color: blue
    em.set_thumbnail(url=member.avatar_url)
    em.set_footer(text='ID: ' + str(member.id))

    # Send message with embed
    await bot.send_message(discord.Object(int(chan)), embed=em)


# Launch
if __name__ == '__main__':
    try:
        # If there's no database file, copy from the empty one
        if not os.path.isfile('data.db'):
            copyfile('data.db.dist', 'data.db')

        # Connect to SQLite3 DB
        db = sqlite3.connect('data.db')
        with db:
            cur = db.cursor()
            cur.execute('SELECT SQLITE_VERSION()')
            data = cur.fetchone()
            print('Version d\'SQLite : %s' % data)

    except Exception as e:
        print("Erreur : " + str(e))
        exit(1)

    bot.run(conf.get('bot', 'token'))
