import random
import discord
import requests
import configparser

from derpibooru import Search, query as dquery
from discord.ext import commands
from botutils import random_line_url, html_md


class Fun:
    """Commandes pour le fun."""
    def __init__(self, bot):
        print('Commandes fun chargés...')
        self.bot = bot
        self.conf = configparser.ConfigParser()
        self.conf.read('./config.ini')

    @commands.command(description='Juste un petit rappel...')
    async def fr(self):
        """Sortir le Bescherelle."""
        fr_em = discord.Embed(colour=0xE5313C)  # color: red-bescherelle
        fr_em.set_image(url='https://i.imgur.com/GjHXdhE.jpg')
        await self.bot.say(embed=fr_em)

    @commands.command(description='Nope nope nope nope nope nope...')
    async def nope(self):
        """Haha, nope !"""
        nope_em = discord.Embed(colour=0x7289DA)  # color: blue
        nope_em.set_image(url=random_line_url('https://nope.kdy.ch/nope.php'))
        await self.bot.say(embed=nope_em)

    @commands.command(description='Hmm.')
    async def ok(self):
        """OK."""
        ok_em = discord.Embed(colour=0x7289DA)  # color: blue
        ok_em.set_image(url=random_line_url('https://nope.kdy.ch/ok.php'))
        await self.bot.say(embed=ok_em)

    @commands.command(description='De belles paroles venant de bons personnages.')
    async def quote(self):
        """Envoie une phrase de la série."""
        quote = requests.get('https://ponyfrance.net/quote.php').content
        quote_em = discord.Embed(description=html_md(quote.decode('utf-8')), colour=0x7289DA)  # color: blue
        await self.bot.say(embed=quote_em)

    @commands.command(description='Pony pony pony!')
    async def derpi(self):
        """Envoie une image random de Derpibooru."""
        derpi_search = Search().key(self.conf.get('bot', 'derpikey')).limit(100)
        derpi_params = {
            'safe, !flash sentry, !spoiler*, !exploitable meme',
            dquery.score >= 100
        }
        derpi_list = [image for image in derpi_search.query(*derpi_params)]
        derpi_image = random.choice(derpi_list)

        derpi_em = discord.Embed(title='#' + str(derpi_image.id), url=derpi_image.url, colour=0x7289DA)  # color: blue
        derpi_em.set_image(url=derpi_image.image)
        await self.bot.say(embed=derpi_em)

    @commands.command(description='YOU CAN\'T SEE ME, YOUR TIME IS NOW.')
    async def cena(self):
        """?"""
        cena_search = Search().key(self.conf.get('bot', 'derpikey')).limit(100)
        cena_params = {
            'john cena, safe, !artist:woox, !word life, !mario, !memecenter, !artist:bronyboy21',
            dquery.score >= 5
        }
        cena_list = [image for image in cena_search.query(*cena_params)]
        cena_image = random.choice(cena_list)

        cena_em = discord.Embed(title='AND HIS NAME IS...', description='**JOHN CENA!**',
                                url=cena_image.url, colour=0x00752F)  # color: green-cena
        cena_em.set_image(url=cena_image.image)
        await self.bot.say(embed=cena_em)

    @commands.command(description='Désolé, j\'ai qu\'un d6')
    async def dé(self):
        """Tire un dé."""
        await self.bot.say('J\'ai tiré un %d !' % random.randint(1, 6))


def setup(bot):
    bot.add_cog(Fun(bot))
