from botutils import del_message
from discord.ext import commands


class Utilitaires:
    """Commandes pratiques."""
    def __init__(self, bot):
        print('Commandes pratiques chargés...')
        self.bot = bot

    @commands.command(pass_context=True, description='Aussi nommé le tenis de table.')
    async def ping(self, ctx):
        """PONG!"""
        author = ctx.message.author
        try:
            await self.bot.say('%s: PONG !' % author.mention)
            await del_message(self, ctx)
        except Exception as e:
            print('>>> ERROR Ping ', e)


def setup(bot):
    bot.add_cog(Utilitaires(bot))
