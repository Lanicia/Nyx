import discord

from botutils import is_mod, is_admin, del_message
from discord.ext import commands


class Mod:
    """Commandes de modération."""
    def __init__(self, bot):
        print('Commandes de modération chargés...')
        self.bot = bot

    @commands.command(pass_context=True, description='Spécifiez un nombre ou je supprime les 50 derniers messages.')
    async def rm(self, ctx, nbr: int = 50):
        """Supprimer des messages."""
        if not is_mod(ctx.message):
            return

        try:
            await self.bot.send_typing(ctx.message.channel)
            deleted = await self.bot.purge_from(ctx.message.channel, before=ctx.message, limit=nbr)

            if len(deleted) > 2:
                await self.bot.say('%d messages supprimés.' % len(deleted))

            await del_message(self, ctx)
        except Exception as e:
            print('>>> ERROR rm ', e)

    @commands.command(pass_context=True, description='TO THE MOOOOOON!')
    async def moon(self, ctx, *, user: discord.Member=None):
        """Kick une personne sur la lune."""
        if not is_mod(ctx.message):
            return

        if user is None:
            await self.bot.say('Il me faut un nom pour envoyer quelqu\'un sur la lune !')
        elif user.id == self.bot.user.id:
            await self.bot.say('Hey !')
        else:
            await self.bot.say('_\\*Prends %s et vise la lune\\*_ TO THE MOOOOOON! <:sillyluna:302196649470459904>' % user.mention)
            await self.bot.kick(user)

        await del_message(self, ctx)

    @commands.command(pass_context=True, description='Zou, to the Sun!')
    async def sun(self, ctx, *, user: discord.Member=None):
        """Bannir une personne sur le soleil."""
        if not is_mod(ctx.message):
            return

        if user is None:
            await self.bot.say('Il me faut un nom pour envoyer quelqu\'un sur le soleil !')
        elif user.id == self.bot.user.id:
            await self.bot.say('Hey !')
        else:
            await self.bot.say('_\\*Prends %s et vise le soleil\\*_ Burn baby, **BURN!** <:srsly:302201014952394754>' % user.mention)
            await self.bot.ban(user)

        await del_message(self, ctx)

    @commands.command(pass_context=True, description='Promis, je reviens !')
    async def reboot(self, ctx):
        """Redémarrer le bot."""
        if not is_admin(ctx.message):
            return

        await del_message(self, ctx)
        exit(0)


def setup(bot):
    bot.add_cog(Mod(bot))
